CREATE DATABASE IF NOT EXISTS testdb;

USE testdb;

CREATE TABLE IF NOT EXISTS `sessions` (
	`session_id` varchar(64) NOT NULL,
	`session_data` text NOT NULL,
	`session_modified` timestamp NOT NULL,
	PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;