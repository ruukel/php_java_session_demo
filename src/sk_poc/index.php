<?php
require_once __DIR__.'/session.php';

if (isset($_POST['addToSession'])) {
	$_SESSION['session_value'] = $_POST['addToSession'];
	header('Location: /sk_poc/');
}

?>
<a href="/sk_poc/java/">Java page</a>
<br><br>

Session:
<pre>
<?php print_r($_SESSION) ?>
</pre>

<br><br>

<form method="post" action="">
	<label>
		<input type="text" name="addToSession" placeholder="Session value">
	</label>
	<br>
	<input type="submit" value="Add to session">
</form>

