<?php

class DbSessionHandler implements SessionHandlerInterface {

	/** @var PDO */
	private $db;

    public function close() {
       unset($this->db);
       return true;
    }

    public function destroy($session_id) {
    	try {
			$stmt = $this->db->prepare('DELETE FROM sessions WHERE session_id = ?');
			$stmt->execute([$session_id]);
			return true;
		} catch (PDOException $e) {
    		return false;
		}
    }

    public function gc($maxlifetime) {
        $datetime = date('Y-m-d H:i:s', time() - $maxlifetime);
        $this->db->exec('DELETE FROM sessions WHERE session_modified < "'.$datetime.'"');

        return true;
    }

    public function open($save_path, $name) {
    	$this->db = new PDO('mysql:host=0.0.0.0;dbname=testdb', 'root', 'testdb');

    	return true;
    }

    public function read($session_id) {
    	$stmt = $this->db->prepare('SELECT session_data FROM sessions WHERE session_id = ?');
        $stmt->execute([$session_id]);

        if ($stmt->rowCount() > 0) {
			return serialize(json_decode($stmt->fetchColumn(), true));
		}

		return '';
    }

    public function write($session_id, $session_data) {
    	$session_data = json_encode(unserialize($session_data)); //Convert to JSON for easier sharing with Java
        $stmt = $this->db->prepare('REPLACE INTO sessions SET session_id = ?, session_data = ?, session_modified = NOW()');
        $stmt->execute([$session_id, $session_data]);

		return true;
    }
}

ini_set('session.serialize_handler', 'php_serialize'); //Use regular serialize() instead of custom php serialize
$handler = new DbSessionHandler();
session_set_save_handler($handler, true);

session_start();

