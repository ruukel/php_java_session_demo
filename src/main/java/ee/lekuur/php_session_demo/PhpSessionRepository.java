package ee.lekuur.php_session_demo;

import org.springframework.data.repository.CrudRepository;

public interface PhpSessionRepository extends CrudRepository<PhpSessionEntity, String> {
}
