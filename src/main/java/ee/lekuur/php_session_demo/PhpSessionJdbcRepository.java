package ee.lekuur.php_session_demo;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.ExpiringSession;
import org.springframework.session.MapSession;
import org.springframework.session.Session;
import org.springframework.session.SessionRepository;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Repository
public class PhpSessionJdbcRepository implements SessionRepository<ExpiringSession> {

	@Autowired
	private PhpSessionRepository phpSessionRepository;

	@Override
	public ExpiringSession createSession() {
		System.out.println("Creating session");
		return new MapSession();
	}

	@Override
	public void save(ExpiringSession session) {
		PhpSessionEntity entity = new PhpSessionEntity();
		entity.setSessionId(session.getId());
		entity.setSessionModified(new Timestamp(session.getLastAccessedTime()));
		entity.setSessionData(convertSessionDataToJson(session));

		System.out.println("Saving session");
		System.out.println(entity);

		phpSessionRepository.save(entity);
	}

	@Override
	public ExpiringSession getSession(String id) {
		PhpSessionEntity entity = phpSessionRepository.findOne(id);

		if (entity == null) {
			System.out.println("Session not found: " + id);
			return null;
		}

		System.out.println("Session found");
		System.out.println(entity);

		ExpiringSession session = new MapSession(entity.getSessionId());
		session.setLastAccessedTime(entity.getSessionModified().getNanos() / 1000000L);

//		if (session.isExpired()) {
//			System.out.println("Session expired");
//			delete(entity.getSessionId());
//			return null;
//		}

		convertSessionDataFromJson(session, entity.getSessionData());

		return session;
	}

	@Override
	public void delete(String id) {
		phpSessionRepository.delete(id);
	}

	private String convertSessionDataToJson(Session session) {
		Map<String, Object> sessionData = new HashMap<>();
		for (String attr : session.getAttributeNames()) {
			sessionData.put(attr, session.getAttribute(attr));
		}

		ObjectMapper mapper = new ObjectMapper();

		try {
			return mapper.writeValueAsString(sessionData);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "{}";
		}
	}

	private void convertSessionDataFromJson(Session session, String json) {
		ObjectMapper mapper = new ObjectMapper();

		try {
			Map map = mapper.readValue(json, Map.class);
			for (Object key : map.keySet()) {
				session.setAttribute((String) key, map.get(key));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
