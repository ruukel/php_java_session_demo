package ee.lekuur.php_session_demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.ExpiringSession;
import org.springframework.session.SessionRepository;
import org.springframework.session.web.http.CookieHttpSessionStrategy;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.session.web.http.HttpSessionStrategy;
import org.springframework.session.web.http.SessionRepositoryFilter;

@Configuration
public class HttpSessionConfig {

	@Bean
	public HttpSessionStrategy httpSessionStrategy() {
		System.out.println("httpSessionStrategy()");
		CookieHttpSessionStrategy strategy = new CookieHttpSessionStrategy();

		DefaultCookieSerializer serializer = new DefaultCookieSerializer();
		serializer.setCookieName("PHPSESSID");
		strategy.setCookieSerializer(serializer);

		return strategy;
	}

	@Bean
	public SessionRepository<ExpiringSession> sessionRepository() {
		System.out.println("sessionRepository()");
		return new PhpSessionJdbcRepository();
	}

	@Bean
	public SessionRepositoryFilter<ExpiringSession> sessionRepositoryFilter(
			SessionRepository<ExpiringSession> sessionRepository,
			HttpSessionStrategy httpSessionStrategy
	) {
		System.out.println("sessionRepositoryFilter()");
		SessionRepositoryFilter<ExpiringSession> sessionRepositoryFilter = new SessionRepositoryFilter<>(sessionRepository);
		sessionRepositoryFilter.setHttpSessionStrategy(httpSessionStrategy);
		return sessionRepositoryFilter;
	}

}
