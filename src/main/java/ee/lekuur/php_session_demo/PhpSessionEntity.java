package ee.lekuur.php_session_demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "sessions")
public class PhpSessionEntity {

	@Id
	@Column(name = "session_id")
	private String sessionId;

	@Column(name = "session_data")
	private String sessionData;

	@Column(name = "session_modified")
	private Timestamp sessionModified;

	public PhpSessionEntity() {
	}

	public PhpSessionEntity(String sessionId, String sessionData, Timestamp sessionModified) {
		this.sessionId = sessionId;
		this.sessionData = sessionData;
		this.sessionModified = sessionModified;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSessionData() {
		return sessionData;
	}

	public void setSessionData(String sessionData) {
		this.sessionData = sessionData;
	}

	public Timestamp getSessionModified() {
		return sessionModified;
	}

	public void setSessionModified(Timestamp sessionModified) {
		this.sessionModified = sessionModified;
	}

	@Override
	public String toString() {
		return sessionId + ": " + sessionData;
	}
}
