package ee.lekuur.php_session_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@SpringBootApplication
@Controller
public class PhpSessionDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhpSessionDemoApplication.class, args);
	}

	@GetMapping("/")
	@ResponseBody
	public String getRoot(HttpSession session) {
		System.out.println("getRoot()");

		String html = "<a href=\"/sk_poc/\">PHP page</a><br><br>";

		Object sessionValue = session.getAttribute("session_value");
		if (sessionValue != null) {
			html += "From session: " + sessionValue;
		} else {
			session.setAttribute("session_value", "default java value");
			html += "Set default session value";
		}

		return html;
	}

}
